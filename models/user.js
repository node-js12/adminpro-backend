const { Schema, model } = require('mongoose')

const UserSchema = Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String

    },
    img: {
        type: String
    },
    role: {
        type: String,
        enum : ['USER_ROLE','ADMIN_ROLE'],
        required: true,
        default: 'USER_ROLE'
    },
    google: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    },
    date: {
        type: Date,
        default: new Date()
    }
})

UserSchema.method('toJSON', function () {
    const {__v, _id , password, ...obj}  =  this.toObject();

    obj.id = _id

    return obj
})

module.exports = model('User', UserSchema)