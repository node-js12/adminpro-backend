const express = require('express')
const cors = require('cors')

const path = require('path');

const dbConnection  = require("../db/config")


class Server {
    constructor(){
        this.connectionDB = this.connectionDB()
        this.app = express()
        this.middlewares()
        this.port = process.env.PORT
        this.paths = {
            users: '/api/users',
            auth: '/api/login',
            hospitales: '/api/hospitales',
            medicos: '/api/medicals',
            search: '/api/search', 
            upload: '/api/uploads',
        }
        this.routes()
    
    }

    middlewares(){
        this.app.use( cors() )
        // lectura del body
        this.app.use( express.json() )

        // set files static
        this.app.use( express.static('public') )
    }

    connectionDB(){
        dbConnection()
    }

    routes(){
        this.app.use(this.paths.users, require('../routes/users'))
        this.app.use(this.paths.auth, require('../routes/auth'))
        this.app.use(this.paths.hospitales, require('../routes/hospitales'))
        this.app.use(this.paths.medicos, require('../routes/medicos'))
        this.app.use(this.paths.search, require('../routes/search'))
        this.app.use(this.paths.upload, require('../routes/uploads'))


        this.app.use('/*', (req, res) => {
            res.sendFile( path.join(__dirname + '/../public/index.html'));
        })
    }

    listen() {
        this.app.listen(this.port, ()=> {
            console.log(`Runnig in http://localhost:${this.port}/`)
        })
        
        
    }
}

module.exports = Server