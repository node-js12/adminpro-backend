const { subirArchivo, updateFile, verifyIfExistBd } = require('../helpers/uploadFile');
const { response } = require('express');
const fs = require('fs');
const path = require('path');

const colectionsAllowed = [
    'users',
    'hospitals',
    'medicals'
]

const uploadFile = async (req, res = response) => {

    const { id , coleccion } = req.params
    try {
        if (!colectionsAllowed.includes(coleccion)) {
            return res.status(404).json({ 'msg': "not found collection, collections allowed are " + colectionsAllowed })
        }
        if (!req.files || Object.keys(req.files).length === 0) { // validar si viene un archivo
            return res.status(400).json({ msg: 'There is not file in the request'});
        }
        // verificar que exista en BD
        const model = await verifyIfExistBd(coleccion, id)

        
        // procesar img 
        const { archivo } = req.files

        // upload file
        const {ok, msg, data} = await subirArchivo(archivo, coleccion)

        if (!ok){ 
            res.status(400).json({ok, msg});
        } else {
            // update file
            updateFile(coleccion, data , model)
            res.status(200).json({ok, msg:"la imagen se subió correctamente", data});
        }

        
    } catch (err) {
        return res.status(400).json(err)
    }
}

const returnImg = async (req, res = response) => {
    const {coleccion, photo} = req.params
    const pathImg = path.join(__dirname + `/../uploads/${coleccion}/${photo}`)

    if (fs.existsSync(pathImg)) {
        res.sendFile(pathImg)
    } else {
        res.sendFile(path.join(__dirname + `/../uploads/no-image.png`))
    }
}

module.exports = {
    returnImg, uploadFile
}