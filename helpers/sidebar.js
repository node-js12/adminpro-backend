

const checkSideBarMenu = (role = 'USER_ROLE') => {
    let menu = [
            {
                titulo: "Dashboard",
                icono: "mdi mdi-gauge",
                submenu: [
                    { titulo: 'Main', url: '/' },
                    { titulo: 'ProgressBar', url: '/progress' },
                    { titulo: 'Graficas', url: '/grafica1' },
                    { titulo: 'Promises', url: '/promises' },
                    { titulo: 'Rxjs', url: 'rxjs' },
                ]
            },
            {
                titulo: "Maintenances",
                icono: "mdi mdi-folder-lock-open",
                submenu: [
                    { titulo: 'Users', url: '/users' },
                    { titulo: 'Hospitals', url: '/hospitals' },
                    { titulo: 'Medicals', url: '/medicals' },
                ]
            }
        ]
    if (role === 'ADMIN_ROLE'){
        return menu
    } else {
        menu[1].submenu.shift()
        return menu
    }
}


module.exports = {checkSideBarMenu}