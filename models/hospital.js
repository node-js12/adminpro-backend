const { Schema, model } = require('mongoose')

const HospitalSchema = Schema({
    name: {
        type: String,
        required: true
    },
    img: {
        type: String
    },
    user:{
        required:true,
        type: Schema.Types.ObjectId,
        ref: 'User' 
    },
    status: {
        type: Boolean,
        default: true
    },
    date: {
        type: Date,
        default: new Date()
    }
}, { collection: 'hospitales' });

HospitalSchema.method('toJSON', function () {
    const { __v, _id, ...obj } = this.toObject();

    obj.id = _id

    return obj
})

module.exports = model('Hospital', HospitalSchema)