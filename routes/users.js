const { Router } = require('express')
const { getUsers,getUser, postUser, putUser, deleteUser} = require('../controllers/users')
const { check } = require('express-validator')
const { validateFields, userIsAdmin, userIsAdminOrIsSame } = require('../middlewares/validate-fields')
const { existEmail, existUserById, roleIsValid} = require('../helpers/db-validators')
const { validarJWT } = require('../middlewares/validar-jwt')

const router = new Router();

router.get('/', [
    validarJWT,
    userIsAdmin
], getUsers )


router.get('/:id', [
    validarJWT,
    userIsAdminOrIsSame,
    check('id', 'Not is valid mongo id!').isMongoId(),
    check('id').custom(existUserById),
    validateFields
], getUser )


router.post('/',  [
    validarJWT,
    userIsAdmin,
    check('role').custom(roleIsValid),
    check('name','El nombre es obligatorio!').notEmpty(),
    check('password','La contraseña es obligatoria!').notEmpty(),
    check('email', 'El email no es valido y es obligatorio!').isEmail(),
    check('email').custom( existEmail ),
    validateFields
], postUser )


router.put('/:id', [
    validarJWT,
    userIsAdminOrIsSame,
    check('role').custom(roleIsValid),
    check('id', 'Not is valid mongo id!').isMongoId(),
    check('id').custom(existUserById),
    validateFields
], putUser)

router.delete('/:id', [
    validarJWT,
    userIsAdmin,
    check('id', 'Not is valid mongo id!').isMongoId(),
    check('id').custom(existUserById),
    validateFields
], deleteUser)


module.exports = router