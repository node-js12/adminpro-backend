const { Router } = require('express')
const { login, signGoogle, rnvToken } = require('../controllers/auth')
const { check } = require('express-validator')
const { validateFields } = require('../middlewares/validate-fields')
const { validarJWT } = require('../middlewares/validar-jwt')



const router = new Router()

router.post('/',[
    check('email', 'El email no es valido y es obligatorio!').isEmail(),
    check('password', 'La contraseña es obligatoria!').notEmpty(),
    validateFields

], login)

router.post('/google',[
    check('token', 'El campo token es obligatorio!').notEmpty(),
    validateFields  

], signGoogle)

/** rnvtoken  , Renovar token */
router.get('/rnvtoken',validarJWT, rnvToken)

module.exports = router