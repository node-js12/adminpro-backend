const { response } = require("express")
const Hospital = require('../models/hospital')
const errorcode = require('../helpers/error-code')

const getHospitales = async(req, res = response) => {

    const { limite = 5, page = 0 } = req.query

    const [total, hospitales] = await Promise.all([
        Hospital.countDocuments({ status: true }),
        Hospital.find()
            .skip(parseInt(page))
            .limit(parseInt(limite))
            .populate('user', 'name img')
            .sort([['date', -1]])
    ])       

    res.json({
        ok:true,
        total,
        hospitales
    })
}

   
const postHospital = async (req, res = response) => {

    try {
        req.body.user = req.uid
        const hosiptal = new Hospital(req.body)
        await hosiptal.save()

        res.json({
            ok: true,
            hosiptal
        })
        
    } catch (error) {
        errorcode(res, error)
    }
    
}

const putHospital = async (req, res = response) => {

    try {
        const uid = req.uid
        const id = req.params.id
                
        const changes = {
            ...req.body,
            user: uid
        }

        const hosiptal = await Hospital.findByIdAndUpdate(id, changes, {new:true})

        res.json({
            ok: true,
            hosiptal
    })
        
    } catch (error) {
        errorcode(res, error)
    }

}

const deleteHospital = async (req, res = response) => {
    const id = req.params.id
    const hospital  = await Hospital.findByIdAndDelete(id)

    res.json({
        ok: true,
        hospital
    })
}



module.exports = {
    getHospitales,
    postHospital,
    putHospital,
    deleteHospital
}