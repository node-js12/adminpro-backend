const { Router } = require('express')
const { getHospitales, postHospital, putHospital, deleteHospital } = require('../controllers/hospitales')
const { check } = require('express-validator')
const { validateFields } = require('../middlewares/validate-fields')
const { existEmail, existUserById, existHospitalById } = require('../helpers/db-validators')
const { validarJWT } = require('../middlewares/validar-jwt')

const router = new Router();

router.get('/', [], getHospitales)

router.post('/', [
    validarJWT,
    check('name', 'El nombre del hosiptal es requerido').notEmpty(),
    validateFields

], postHospital)

router.put('/:id', [
    validarJWT,
    check('id', 'Mongo id Invalido').isMongoId(),
    check('id').custom(existHospitalById),
    validateFields
], putHospital)

router.delete('/:id', [
    validarJWT,
    check('id', 'Mongo id Invalido').isMongoId(),
    check('id').custom(existHospitalById),
    validateFields

], deleteHospital)


module.exports = router