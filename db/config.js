const mongoose = require('mongoose')
require('colors')

const uri = `${process.env.DATABASE_URI}/hospital`

const dbConnection = async () => {

        try {
            await mongoose.connect(uri, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
                useCreateIndex: true
            });
            console.log("Database online".blue)
        } catch (error) {
            console.error(error)
            throw new Error("Error al levantar la BD..".red)
        }



}

module.exports = dbConnection