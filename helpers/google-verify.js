const { OAuth2Client } = require('google-auth-library');

// get cliente google
const client = new OAuth2Client(process.env.ID_CLIENT);

// verify valid token cliente
const verifyGoogle = async(idToken = '') => {
  
  
  const ticket = await client.verifyIdToken({
    idToken,
    audience: process.env.ID_CLIENT,  
  });
  return {email, name, picture} = ticket.getPayload();
}


module.exports = {
    verifyGoogle
}